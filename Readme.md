# Office #

Este documento tem como objetivo de auxiliar na documentação deste projeto.

## Recursos do projeto ##

* [Comandos](./commands/Readme.md)
* Components
* Documentação
* Teste

## Como Instalar ##

1° Adicone o `@office` em suas `dependencies` de seu `package.json`.

```json
    //package.json
    {
        //...
        "@office": "git+https://enriqueprieto65@bitbucket.org/tresw/office.git"
        //...
    }
```

2° Salve seu `package.js` e instale o pacote.

```prompt
   ~/ $ npm install
```

Pronto.
