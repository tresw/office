# Comandos #

## Como Usar ##

```shell
##Localmente
$ node_modules/@office/node office <command>

##Globalmente
$ office <command>
```

### Registrar o comando de forma global ###

```shell
##
$ node_modules/@office/npm link
```

---

## Lista de Comandos ##

### config -c ###

Inicializa ou edita um arquivo de configuração na raiz do projeto.

```shell
##Command
$ office config

##Alias
$ office -c
```

Um arquivo **office.config.json** é criado.

```json
{
    "name": "Nome do Projeto",
    "path": "path/to/project",
    "framework": "Vue.js | Ionic"
}
```
