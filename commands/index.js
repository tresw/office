#!/usr/bin/env node

const program = require('commander');
const package = require('../package.json');

/**
 * Office Commands List
 */
/** @command config -c */
require("./config/config");
    

program.version(package.version);
program.parse(process.argv);