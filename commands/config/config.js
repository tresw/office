#!/usr/bin/env node

const program = require('commander');
const inquirer = require('inquirer');
const path = require('path');
const { join } = require('path');
const chalk = require('chalk');
const fs = require('fs');

/** 
 * Define o nome, alias e a descrição do comando.
 * 
 * @constant { Object } data
 * @var { string } data.name
 * @var { string } data.alias
 * @var { string } data.description
 * */
const data = {
    name: "config",
    alias: "-c",
    description: "Criar ou editar arquivo de configuração do office."
};

/** 
 * Retorna uma string com o caminho do diretório raiz do projeto.  
 * 
 * @constant { string } root */
const root = (function(){
    return path.dirname(require.main.filename || process.mainModule.filename);
})();

/**
 * @constant filename Retorna o nome do arquivo.
 * @constant filepath Retorna o caminho com o nome do arquivo concatenado.
 */
const filename = "office.config.json";
const filepath = join(root, filename);


/**
 * @function prompt Captura as informações para criar o arquivo office.config.json
 * @function confirm Captura as informações para saber vai editar o arquivo de configuração
 * @function write Captura as informações para saber vai editar o arquivo de configuração
 */
function prompt(config=null){
    let inputs = [];
    /**
     * Define a entrada de um texto que será o nome do projeto.
     */
    inputs.push({
        type: 'input',
        name: 'name',
        default: (config)?config.name:null,
        message: `${chalk.green('Qual o nome do Projeto?')}`,
        validate: (value)=>{ return value?true:`${chalk.red('É preciso digitar o nome do projeto.')}` }
    });
    /**
     * Define a entrada de um texto que será o caminho do diretório do projeto.
     */
    inputs.push({
        type: 'input',
        name: 'path',
        default: (config)?config.path:null,
        message: `${chalk.green('Qual é o caminho do diretório que deseja deixar configurado o Office App?')}`,
        validate: (value)=>{ return value?true:`${chalk.red('É preciso digitar o caminho do diretório.')}` }
    });
    /**
     * Define a entrada da escolha de uma opção do Framework que será configurado
     */
    inputs.push({
        type: 'list',
        choices: ['Vue.js', 'Ionic'],
        default: (config)?config.framework:null,
        name: 'framework',
        message: `${chalk.green('Qual é o framework que será configurado?')}`,
        validate: (value)=>{ return value?true:`${chalk.red('É preciso selecionar um framework.')}` }
    });
    return inquirer.prompt(inputs);
}
async function confirm(msg="Deseja continuar? (s/n)"){
    let inputs = [],
        pos = ["sim", "s", "si", "yes"];
    inputs.push({
        type: 'input',
        name: 'confirm',        
        message: `${chalk.green(msg)}`
    });
    let prompt = await inquirer.prompt(inputs);
    if(prompt.confirm){
        if(pos.includes(prompt.confirm)){
            return true;
        }
    }
    return false;
}
function write(config){
    fs.writeFileSync(filepath, JSON.stringify(config, null, '\t'), { flag: 'w' });
}


program.command(data.name).alias(data.alias).description(data.description).action(async ()=>{
    let config, edit, save;
    if(fs.existsSync(filepath)){
        console.log('Arquivo já existe');
        let file = JSON.parse(fs.readFileSync(filepath));
        edit = await confirm();   
        if(edit){
            config = await prompt(file);
        }else{
            return;
        }
    }else{
        config = await prompt();
    }
    save = await confirm("Deseja salvar o arquivo de configuração? (s/n)");
    if(save){
        write(config);
    }
});